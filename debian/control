Source: umap-learn
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3,
               python3-setuptools,
               python3-numpy,
               python3-scipy,
               python3-sklearn,
               python3-numba,
               python3-pynndescent <!nocheck>,
               python3-tqdm <!nocheck>,
               python3-pytest <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/umap-learn
Vcs-Git: https://salsa.debian.org/med-team/umap-learn.git
Homepage: https://github.com/lmcinnes/umap
Rules-Requires-Root: no

Package: umap-learn
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-numpy,
         python3-scipy,
         python3-sklearn,
         python3-numba,
         python3-pandas
Description: Uniform Manifold Approximation and Projection
 Uniform Manifold Approximation and Projection (UMAP) is a dimension
 reduction technique that can be used for visualisation similarly to t-
 SNE, but also for general non-linear dimension reduction. The algorithm
 is founded on three assumptions about the data:
 .
  1. The data is uniformly distributed on a Riemannian manifold;
  2. The Riemannian metric is locally constant (or can be
     approximated as such);
  3. The manifold is locally connected.
 .
 From these assumptions it is possible to model the manifold with a fuzzy
 topological structure. The embedding is found by searching for a low
 dimensional projection of the data that has the closest possible
 equivalent fuzzy topological structure.
